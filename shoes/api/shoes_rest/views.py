from django.shortcuts import render
from shoes_rest.models import Shoe, BinVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
# Create your views here.
class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacture",
        "name",
        "color",
        "photo",
        "bin",

    ]
    encoders={

        "bin":BinVODetailEncoder(),
    }


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacture",
        "name",
        "id",
        "color",
        "photo",]
    encoders={

        "bin":BinVODetailEncoder(),
    }
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}






@require_http_methods(["GET","POST"])
def api_list_shoes(request, bin_vo_id=None):

    if request.method == "GET":
        if bin_vo_id is not None:
            shoe = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoe = Shoe.objects.all()
        return JsonResponse({"shoes":shoe},encoder=ShoeListEncoder)
    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "invalid ID"},status=400)

        shoe = Shoe.objects.create(**content)
        return JsonResponse(shoe,encoder=ShoeDetailEncoder,safe=False)

@require_http_methods(["DELETE","GET"])
def api_show_shoe(request, id):
    shoe = Shoe.objects.get(id=id)
    if request == "GET":
        return JsonResponse(
            {"shoe": shoe},encoder=ShoeDetailEncoder,
        )
    else:
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
