from django.db import models


# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    def __str__(self):
        return f"{self.closet_name}"
class Shoe(models.Model):
    manufacture = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    color = models.CharField(max_length=40)
    photo = models.URLField()

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True)

    def __str__(self):
        return f"{self.name}"
