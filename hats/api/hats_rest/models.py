from django.db import models


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, null=True, unique=True)
    closet_name = models.CharField(max_length=100, null=True)

    def __str__(self):
        return f"location: {self.closet_name}"


class Hat(models.Model):
    style = models.CharField(max_length=50)
    color = models.CharField(max_length=25)
    fabric = models.CharField(max_length=50, null=True)
    size = models.CharField(max_length=10)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(LocationVO, related_name="hats", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f"{self.style}"
