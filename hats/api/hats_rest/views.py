import json
from common.json import ModelEncoder
from django.http import JsonResponse
from hats_rest.models import Hat, LocationVO
from django.views.decorators.http import require_http_methods


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
    ]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style",
        "color",
        "fabric",
        "size",
        "picture_url",
        "location",

    ]
    encoders={

        "location":LocationVODetailEncoder(),
    }


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style",
        "color",
        "fabric",
        "location",
        ]


    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

@require_http_methods(["GET","POST"])
def api_list_hats(request, location_vo_id=None):

    if request.method == "GET":
        if location_vo_id is not None:
            hat = Hat.objects.filter(location=location_vo_id)
        else:
            hat = Hat.objects.all()
        return JsonResponse({"hats":hat},encoder=HatListEncoder)
    else:
        content = json.loads(request.body)
        print("******THIS IS CONTENT********",content)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location"},status=400)

        hat = Hat.objects.create(**content)
        return JsonResponse(hat,encoder=HatDetailEncoder,safe=False)
@require_http_methods(["DELETE","GET"])
def api_show_hats(request, id):
    hat = Hat.objects.get(id=id)
    if request == "GET":
        return JsonResponse(
            {"hat": hat},encoder=HatDetailEncoder,
        )
    else:
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
