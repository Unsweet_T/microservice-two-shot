import React from 'react'
function ShoeList(props) {
  async function deleteshoe(id){
    const shoeURL = `http://localhost:8080/api/shoes/${id}`
    const fetchConfig = {
      method: "DELETE",
    };
    const response = await fetch(shoeURL, fetchConfig);
    if (response.ok){
      window.locations.reload(false);
    }
  }
  return (
    <div>
      <table className="table table-striped">
          <thead>
              <tr>
              <th>Manufacture</th>
              <th>Name</th>
              <th>color</th>
              <th>Bin</th>
              <th>Picture</th>
              <th>Delete</th>
              </tr>
          </thead>
          <tbody>
              {props.shoes.map(shoe => {
              return (
                  <tr key={shoe.href}>
                  <td>{ shoe.manufacture }</td>
                  <td>{ shoe.name }</td>
                  <td>{ shoe.color }</td>
                  <td>{ shoe.bin }</td>
                  <td><img src={ shoe.photo } alt={shoe.name}/></td>
                  <td><button type="submit" className="btn btn-danger" onClick={() => {deleteshoe(shoe.id)}}>delete</button></td>
                  </tr>
              );
              })}
          </tbody>
      </table>
    </div>
  );
}

export default ShoeList;
