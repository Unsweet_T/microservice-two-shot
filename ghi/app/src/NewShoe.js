import React, { useEffect,useState } from 'react';

function ShoeForm(){
    const [manufacture, setManu] = useState('');
    const [name, setName] = useState('');
    const [color, setColor] = useState('');
    const [bin, setBin] = useState('');
    const [photo, setPic] = useState('');
    const [bins, setBins] = useState([]);

    const handleManu = (event) =>{
        const value = event.target.value;
        setManu(value)
    }
    const handleName = (event) =>{
        const value = event.target.value;
        setName(value)
    }
    const handleColor = (event) =>{
        const value = event.target.value;
        setColor(value)
    }
    const handleBin = (event) =>{
        const value = event.target.value;
        setBin(value)
    }
    const handlePic = (event) =>{
        const value = event.target.value;
        setPic(value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.manufacture = manufacture;
        data.name = name;
        data.color = color;
        data.photo = photo;
        data.bin = bin;

        console.log(data);

        const shoeUrl = "http://localhost:8080/api/shoes/"
        const fetchOptions = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': "application/json",
            },
        };
        const response = await fetch(shoeUrl, fetchOptions);
        if(response.ok){
            const NewShoe = await response.json();
            console.log(NewShoe);
            setManu('');
            setName('');
            setColor('');
            setPic('');
            setBin('');
            }
        }
    const fetchData = async () =>{
        const binurl = 'http://localhost:8100/api/bins/';

        const response = await fetch(binurl);
        if(response.ok){
            const data = await response.json();
            setBins(data.bins)
            }
        }
    useEffect(() => {
        fetchData();
    }, []);
    return(
            <div className="form-row">
                <div className="col">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Shoe</h1>
                        <form onSubmit={handleSubmit} id="create-shoe-form">
                            <div className="form-folating mb-3">
                                <label htmlFor="manufacture">Manufacture</label>
                                <input onChange={handleManu} placeholder="Manufacture" required type="text" name="manufacture" id="manufacture" className="form-control" value={manufacture} />
                            </div>
                            <div className="form-folating mb-3">
                                <label htmlFor="name">Name</label>
                                <input onChange={handleName} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name} />
                            </div>
                            <div className="form-folating mb-3">
                                <label htmlFor="color">Color</label>
                                <input onChange={handleColor}placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color}/>
                            </div>
                            <div className="form-folating mb-3">
                                <label htmlFor="photo">Picture</label>
                                <input onChange={handlePic} placeholder="Picture URL" required type="text" name="photo" id="photo" className="form-control" value={photo} />
                            </div>
                            <div className="mb-3">
                                <select onChange={handleBin} required name="bin" id="bin" className="form-select" value={bin}>
                                    <option value="">Choose a bin</option>
                                    {bins.map(bin =>{
                                    return(
                                        <option key={bin.id} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    )
                                })}
                                </select>
                                <button type="submit" className="btn btn-primary">Submit</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }

export default ShoeForm;
