import React from 'react';

function HatInventory(props) {
  async function deletehat(id){
    const hatURL = `http://localhost:8090/api/hats/${id}`
    const fetchConfig = {
      method: "DELETE",
    };
    const response = await fetch(hatURL, fetchConfig);
    if (response.ok){
      window.locations.reload(false);
    }
  }
  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Image</th>
            <th>Style</th>
            <th>Color</th>
            <th>Fabric</th>
            <th>Size</th>
         </tr>
      </thead>
      <tbody>
            {props.hats.map(hat => {
            return (
              <tr key={hat.href}>
                <td><img src={ hat.picture_url} alt={hat.style}/></td>
                <td>{ hat.style }</td>
                <td>{ hat.color}</td>
                <td>{ hat.fabric }</td>
                <td>{ hat.size }</td>
                <td><button type="submit" className="btn btn-danger" onClick={() => {deletehat(hat.id)}}>delete</button></td>
              </tr>
              );
              })}
          </tbody>
      </table>
    </div>
  );
}

export default HatInventory;
