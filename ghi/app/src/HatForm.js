import React, { useEffect, useState } from 'react';

function HatForm() {
  const [style, setStyle] = useState('');
  const [color, setColor] = useState('');
  const [size, setSize] = useState('');
  const [fabric, setFabric] = useState('');
  const [picture_url, setPictureUrl] = useState('');
  const [location, setLocation] = useState('');
  const [locations, setLocations] = useState([]);

  const handleStyle = (event) => {
    const value = event.target.value;
    setStyle(value);
  };
  const handleColor = (event) => {
    const value = event.target.value;
    setColor(value);
  };
  const handleSize = (event) => {
    const value = event.target.value;
    setSize(value);
  };
  const handlePictureUrl = (event) => {
    const value = event.target.value;
    setpicture_url(value);
  };
  const handleLocation = (event) => {
    const value = event.target.value;
    setLocation(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      style,
      color,
      size,
      picture_url,
      location,
    };

    console.log(data);

    const hatUrl = "http://localhost:8090/api/hats/";
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': "application/json",
      },
    };
    const response = await fetch(hatUrl, fetchOptions);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);
      setStyle('');
      setColor('');
      setSize('');
      setPictureUrl('');
      setLocation('');
    }
  };

  const fetchLocations = async () => {
    const locationUrl = 'http://localhost:8100/api/locations/';

    const response = await fetch(locationUrl);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchLocations();
  }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a Hat</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleStyle} placeholder="Style" required type="text" name="style" id="style" className="form-control" value={style} />
                        <label htmlFor="style">Style</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleSize} placeholder="Size" required type="text" name="size" id="size" className="form-control" value={size} />
                        <label htmlFor="size">Size</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureUrl} placeholder="Picture URL" required type="text" name="pictureUrl" id="pictureUrl" className="form-control"value={pictureUrl} />
                        <label htmlFor="pictureUrl">Picture URL</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleLocation} required name="location" id="location" className="form-select" value={location}>
                            <option value="">Choose a location</option>
                            {locations.map((location) => {
                            return(
                                <option key={location.id} value={location.href}>
                                    {location.closet_name}
                                </option>
                            )
                        })}
                        </select>
                    </div>
                    <button type="submit" className="btn btn-primary">
                        Submit
                    </button>
                </form>
            </div>
        </div>
    </div>
);
}

export default HatForm;
