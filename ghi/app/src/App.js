import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList'
import ShoeForm from './NewShoe';
import HatForm from "./HatForm";
import HatInventory from "./HatInventory";

function App(props) {
  // if(props.shoes === undefined && props.hats === undefined){
  //   return null;
  // }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes/" element={<ShoeList shoes={props.shoes}/>}/>
          <Route path="new/" element={<ShoeForm />}/>
          <Route path="hats/" element={<HatInventory hats={props.hats} />} />
          <Route path="newhat/" element={<HatForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
