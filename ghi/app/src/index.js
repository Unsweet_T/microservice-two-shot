import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function load_data(){
  const response = await fetch('http://localhost:8080/api/shoes/');
  if(response.ok){
    const data = await response.json();
    root.render(
      <React.StrictMode>
        <App shoes={data.shoes}/>
      </React.StrictMode>
    )
  }
}
load_data();

async function load_hat_data(){
  const response = await fetch('http://localhost:8090/api/hats/');
  if(response.ok){
    const data = await response.json();
    root.render(
      <React.StrictMode>
        <App hats={data.hats}/>
      </React.StrictMode>
    );
  } else {
    console.error("Failed to retrieve hat data:", response.status);
  }
}
load_hat_data();